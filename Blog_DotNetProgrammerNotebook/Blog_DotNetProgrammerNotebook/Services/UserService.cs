﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Services
{
    public class UserService
    {
        UserRepository _userRepository;
        public UserService()
        {
            _userRepository = new UserRepository();
        }
        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _userRepository.GetAllUsersAsync() ?? new List<User>();
        }
        public async Task<User> GetUserByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                return await _userRepository.GetUserByIdAsync(id);
            }
            return null;
        }

        public async Task<User> CreateUserAsync(User user)
        {
            if (user != null 
                && user.FirstName != string.Empty
                && user.LastName != string.Empty
                && user.Email != string.Empty)
            {
                return await _userRepository.CreateUserAsync(user);
            }
            return null;
        }

        public async void DeleteUserByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                await _userRepository.GetUserByIdAsync(id);
            }
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            if (user != null 
                && user.FirstName != string.Empty
                && user.LastName != string.Empty
                && user.Email != string.Empty)
            {
                return await _userRepository.UpdateUserAsync(user);
            }
            return null;
        }
    }
}