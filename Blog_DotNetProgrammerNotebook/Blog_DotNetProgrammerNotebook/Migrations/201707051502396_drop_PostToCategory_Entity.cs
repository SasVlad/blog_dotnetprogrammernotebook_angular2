namespace Blog_DotNetProgrammerNotebook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class drop_PostToCategory_Entity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostToCategories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.PostToCategories", "PostId", "dbo.Posts");
            DropIndex("dbo.PostToCategories", new[] { "CategoryId" });
            DropIndex("dbo.PostToCategories", new[] { "PostId" });
            AddColumn("dbo.Posts", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Posts", "CategoryId");
            AddForeignKey("dbo.Posts", "CategoryId", "dbo.Categories", "Id", cascadeDelete: false);
            DropTable("dbo.PostToCategories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PostToCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Posts", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Posts", new[] { "CategoryId" });
            DropColumn("dbo.Posts", "CategoryId");
            CreateIndex("dbo.PostToCategories", "PostId");
            CreateIndex("dbo.PostToCategories", "CategoryId");
            AddForeignKey("dbo.PostToCategories", "PostId", "dbo.Posts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PostToCategories", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
    }
}
