"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const categoryHttpService_1 = require("../httpServices/categoryHttpService");
let CategoryComponent = class CategoryComponent {
    constructor(categoryHttpService) {
        this.categoryHttpService = categoryHttpService;
        this.categories = [];
    }
    ngOnInit() {
        this.categoryHttpService.getAllCategories().subscribe((data) => this.categories = data);
    }
    createNewCategory() {
        window.location.replace('newCategory');
    }
    GetPostsByCategoryId(data) {
        window.location.replace('postsByCategory/' + data);
    }
};
CategoryComponent = __decorate([
    core_1.Component({
        selector: 'my-category-app',
        template: ` <div class="container">               
                    <div class="panel panel-success col-md-3" >
                        <div>
                            <h2> Catigories: </h2>
                                <div *ngFor="let category of categories">
                                    <input type="button" class="btn btn-primary btn-block btn-lg" (click)="GetPostsByCategoryId(category.Id)" value="{{category.Name}}" />
                                </div>
                                    <input type="button" class="btn btn-success btn-block btn-lg" (click)="createNewCategory()" value="Create category" />
                                </div>    
                        </div>
                      <div class="panel panel-primary col-md-9">
                          <router-outlet></router-outlet>
                      </div>
                </div>`,
        providers: [categoryHttpService_1.CategoryHttpService]
    }),
    __metadata("design:paramtypes", [categoryHttpService_1.CategoryHttpService])
], CategoryComponent);
exports.CategoryComponent = CategoryComponent;
//# sourceMappingURL=categoryComponent.js.map