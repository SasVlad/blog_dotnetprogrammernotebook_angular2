"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const categoryHttpService_1 = require("../httpServices/categoryHttpService");
const postHttpService_1 = require("../httpServices/postHttpService");
const router_1 = require("@angular/router");
let DisplayPostsByCategoryIdComponent = class DisplayPostsByCategoryIdComponent {
    constructor(categoryHttpService, postHttpService, activateRoute) {
        this.categoryHttpService = categoryHttpService;
        this.postHttpService = postHttpService;
        this.activateRoute = activateRoute;
        this.posts = [];
        this.CreatePost = function () {
            window.location.replace('newPost/' + this.categoryId);
        };
        this.categoryId = activateRoute.snapshot.params['categoryId'];
    }
    ngOnInit() {
        //$routeParams.categoryId 
        this.categoryHttpService.getCategoryById(this.categoryId).subscribe((data) => this.categoryName = data.Name);
        this.postHttpService.getAllPostsByCategoryId(this.categoryId).subscribe((data) => this.posts = data);
    }
};
DisplayPostsByCategoryIdComponent = __decorate([
    core_1.Component({
        selector: 'my-postsByCategoryid-app',
        template: `<div>
                    <h2> Catigory: {{categoryName}} </h2>
                    <h3> All Posts: </h3>
                        <div class="panel panel-primary" *ngFor="let post of posts">
                             <div class="panel-heading">
                        <h4>
                            <a style="color:white" href="/currentPost/{{post.Id}}">{{post.Title}}</a>
                        </h4>
                    </div>
                        <div style="padding:10px">{{post.Article}}</div>
                    <div class="panel-footer"><div><b>Views:</b>{{post.Views}} <b>Author:</b>{{post.Author.LastName}} {{post.Author.FirstName}} <b>DatePublished:</b> {{post.DatePublished}}</div></div>
                    </div>
                        <input type="button" class="btn btn-primary btn-block btn-lg" (click)="CreatePost()" value="Create Post" />
                </div>`,
        providers: [categoryHttpService_1.CategoryHttpService, postHttpService_1.PostHttpService]
    }),
    __metadata("design:paramtypes", [categoryHttpService_1.CategoryHttpService, postHttpService_1.PostHttpService, router_1.ActivatedRoute])
], DisplayPostsByCategoryIdComponent);
exports.DisplayPostsByCategoryIdComponent = DisplayPostsByCategoryIdComponent;
//# sourceMappingURL=displayPostsByCategoryIdComponent.js.map