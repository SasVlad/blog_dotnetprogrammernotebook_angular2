﻿import { Component } from '@angular/core';
import { Response } from '@angular/http';
import { CategoryHttpService } from '../httpServices/categoryHttpService';
import { Category } from '../models/category';

@Component({
    selector: 'my-createCategory-app',
    template: `<div class="panel">
                    <div class="panel-heading">
                        <h3>Create new category</h3>
                    </div>
                    <div class="panel-body">
                         <div class="form-group">
                            <label>Category Name:</label>
                            <input style="margin-left: 15px;width:500px" type="text" [(ngModel)]="newCategory.Name" name="tbCategoryName" />
                         </div>        
                         <div style="padding-left:500px">
                            <input type="submit" name="tbSubmitCreateCategory" (click)="createNewCategory(newCategory)" value="Submit" class="btn btn-primary" />
                         </div>
                    </div>
            </div>`,
    providers: [CategoryHttpService]
})
export class CreateCategoryComponent {

    newCategory: Category = new Category();

    constructor(private categoryHttpService: CategoryHttpService) { }

    createNewCategory(newCategory: Category) {
        this.categoryHttpService.careateCategoryData(newCategory)
            .subscribe((data) => { window.location.replace('postsByCategory/' + data.Id )});//window.location.replace('/#!/newCategory');
    }
}