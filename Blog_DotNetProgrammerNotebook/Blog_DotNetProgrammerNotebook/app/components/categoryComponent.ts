﻿import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { CategoryHttpService } from '../httpServices/categoryHttpService';
import { Category } from '../models/category';
import { Router } from '@angular/router';


@Component({
    selector: 'my-category-app',
    template: ` <div class="container">               
                    <div class="panel panel-success col-md-3" >
                        <div>
                            <h2> Catigories: </h2>
                                <div *ngFor="let category of categories">
                                    <input type="button" class="btn btn-primary btn-block btn-lg" (click)="GetPostsByCategoryId(category.Id)" value="{{category.Name}}" />
                                </div>
                                    <input type="button" class="btn btn-success btn-block btn-lg" (click)="createNewCategory()" value="Create category" />
                                </div>    
                        </div>
                      <div class="panel panel-primary col-md-9">
                          <router-outlet></router-outlet>
                      </div>
                </div>`,
    providers: [CategoryHttpService]
})
export class CategoryComponent implements OnInit {

    categories: Category[] = [];

    constructor(private categoryHttpService: CategoryHttpService) { }

    ngOnInit() {
        this.categoryHttpService.getAllCategories().subscribe((data) => this.categories = data);
    }

    createNewCategory() {
        window.location.replace('newCategory');
    }

    GetPostsByCategoryId(data: number)
    {
        window.location.replace('postsByCategory/' + data);
    }
}