﻿import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { CategoryHttpService } from '../httpServices/categoryHttpService';
import { PostHttpService } from '../httpServices/postHttpService';
import { Category } from '../models/category';
import { Post } from '../models/post';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-postsByCategoryid-app',
    template: `<div>
                    <h2> Catigory: {{categoryName}} </h2>
                    <h3> All Posts: </h3>
                        <div class="panel panel-primary" *ngFor="let post of posts">
                             <div class="panel-heading">
                        <h4>
                            <a style="color:white" href="/currentPost/{{post.Id}}">{{post.Title}}</a>
                        </h4>
                    </div>
                        <div style="padding:10px">{{post.Article}}</div>
                    <div class="panel-footer"><div><b>Views:</b>{{post.Views}} <b>Author:</b>{{post.Author.LastName}} {{post.Author.FirstName}} <b>DatePublished:</b> {{post.DatePublished}}</div></div>
                    </div>
                        <input type="button" class="btn btn-primary btn-block btn-lg" (click)="CreatePost()" value="Create Post" />
                </div>`,
    providers: [CategoryHttpService, PostHttpService]
})
export class DisplayPostsByCategoryIdComponent implements OnInit {

    categoryName: string;
    posts: Post[] = [];
    categoryId: number;
    constructor(private categoryHttpService: CategoryHttpService, private postHttpService: PostHttpService, private activateRoute: ActivatedRoute) {
        this.categoryId = activateRoute.snapshot.params['categoryId'];
    }

    ngOnInit() {
        //$routeParams.categoryId 
        this.categoryHttpService.getCategoryById(this.categoryId).subscribe((data) => this.categoryName = data.Name);
        this.postHttpService.getAllPostsByCategoryId(this.categoryId).subscribe((data) => this.posts = data);
    }

    CreatePost = function () {
        window.location.replace('newPost/' + this.categoryId);
    }
}