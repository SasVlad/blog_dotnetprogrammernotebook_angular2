﻿import { Component,OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { PostHttpService } from '../httpServices/postHttpService';
import { UserHttpService } from '../httpServices/userHttpService';
import { CommentHttpService } from '../httpServices/commentHttpService';
import { Post } from '../models/post';
import { User } from '../models/user';
import { Comment } from '../models/comment';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-postsById-app',
    template: `<div class="panel panel-primary">
                    <div class="panel-heading">
                        <div>
                            <h4>
                                {{post.Title}}
                            </h4>
                        </div>
                    </div>
                        <div style="padding:10px">{{post.Article}}</div>
                   <div class="panel-footer"><div><b>Views:</b>{{post.Views}} <b>Author:</b>{{post.Author.LastName}} {{post.Author.FirstName}} <b>DatePublished:</b> {{post.DatePublished}}</div></div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Comments</h3>
                    </div>
                        <div *ngFor="let comment of post.Comments">
                            <div class="panel panel-success">
                                <div style="padding:10px"><b>{{comment.CommentText}}</b></div>
                                <div class="panel-footer"><div><b>User:</b> {{comment.User.LastName}} {{comment.User.FirstName}} <b>DateRecord:</b> {{comment.DateRecord}}</div></div>
                            </div>
                        </div>
                     </div>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4>New comment</h4>
                            </div>
                            <textarea style="height:100px;width:500px" [(ngModel)]="_newComment.CommentText"></textarea>
                                <div>Input First Name:<input type="text" [(ngModel)]="_user.FirstName"/> Input Last Name:<input type="text" [(ngModel)]="_user.LastName" /> 
                                Input Email:<input type="text" [(ngModel)]="_user.Email" /></div>
                                <input type="button" (click)="CreateNewComment(_user,_newComment)" value="Post" />
                     </div>`,
    providers: [PostHttpService, CommentHttpService, UserHttpService]
})
export class DisplayPostByIdComponent implements OnInit {

    post: Post=new Post();
    _user: User = new User();
    postid: number;
    _newComment: Comment = new Comment();
    constructor(private postHttpService: PostHttpService, private commentHttpService: CommentHttpService,
        private userHttpService: UserHttpService, private activateRoute: ActivatedRoute)
    {
        this.postid = activateRoute.snapshot.params['postid'];
        this._newComment.PostId = this.postid;
    }

    ngOnInit() {
        this.postHttpService.getPostById(this.postid).subscribe((data) => this.post = data);
        console.log(this.post);
    }
    CreateNewComment(user: User, comment: Comment) {
        this.userHttpService.careateUserData(user)
            .subscribe((data) => {
                comment.UserId = data.Id;
                this.commentHttpService.createCommentData(comment).subscribe((data) => {
                    window.location.reload();
                })
            });
    }
}