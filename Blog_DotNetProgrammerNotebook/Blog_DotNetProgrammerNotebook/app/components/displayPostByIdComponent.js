"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const postHttpService_1 = require("../httpServices/postHttpService");
const userHttpService_1 = require("../httpServices/userHttpService");
const commentHttpService_1 = require("../httpServices/commentHttpService");
const post_1 = require("../models/post");
const user_1 = require("../models/user");
const comment_1 = require("../models/comment");
const router_1 = require("@angular/router");
let DisplayPostByIdComponent = class DisplayPostByIdComponent {
    constructor(postHttpService, commentHttpService, userHttpService, activateRoute) {
        this.postHttpService = postHttpService;
        this.commentHttpService = commentHttpService;
        this.userHttpService = userHttpService;
        this.activateRoute = activateRoute;
        this.post = new post_1.Post();
        this._user = new user_1.User();
        this._newComment = new comment_1.Comment();
        this.postid = activateRoute.snapshot.params['postid'];
        this._newComment.PostId = this.postid;
    }
    ngOnInit() {
        this.postHttpService.getPostById(this.postid).subscribe((data) => this.post = data);
        console.log(this.post);
    }
    CreateNewComment(user, comment) {
        this.userHttpService.careateUserData(user)
            .subscribe((data) => {
            comment.UserId = data.Id;
            this.commentHttpService.createCommentData(comment).subscribe((data) => {
                window.location.reload();
            });
        });
    }
};
DisplayPostByIdComponent = __decorate([
    core_1.Component({
        selector: 'my-postsById-app',
        template: `<div class="panel panel-primary">
                    <div class="panel-heading">
                        <div>
                            <h4>
                                {{post.Title}}
                            </h4>
                        </div>
                    </div>
                        <div style="padding:10px">{{post.Article}}</div>
                   <div class="panel-footer"><div><b>Views:</b>{{post.Views}} <b>Author:</b>{{post.Author.LastName}} {{post.Author.FirstName}} <b>DatePublished:</b> {{post.DatePublished}}</div></div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>Comments</h3>
                    </div>
                        <div *ngFor="let comment of post.Comments">
                            <div class="panel panel-success">
                                <div style="padding:10px"><b>{{comment.CommentText}}</b></div>
                                <div class="panel-footer"><div><b>User:</b> {{comment.User.LastName}} {{comment.User.FirstName}} <b>DateRecord:</b> {{comment.DateRecord}}</div></div>
                            </div>
                        </div>
                     </div>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h4>New comment</h4>
                            </div>
                            <textarea style="height:100px;width:500px" [(ngModel)]="_newComment.CommentText"></textarea>
                                <div>Input First Name:<input type="text" [(ngModel)]="_user.FirstName"/> Input Last Name:<input type="text" [(ngModel)]="_user.LastName" /> 
                                Input Email:<input type="text" [(ngModel)]="_user.Email" /></div>
                                <input type="button" (click)="CreateNewComment(_user,_newComment)" value="Post" />
                     </div>`,
        providers: [postHttpService_1.PostHttpService, commentHttpService_1.CommentHttpService, userHttpService_1.UserHttpService]
    }),
    __metadata("design:paramtypes", [postHttpService_1.PostHttpService, commentHttpService_1.CommentHttpService,
        userHttpService_1.UserHttpService, router_1.ActivatedRoute])
], DisplayPostByIdComponent);
exports.DisplayPostByIdComponent = DisplayPostByIdComponent;
//# sourceMappingURL=displayPostByIdComponent.js.map