﻿import { Component } from '@angular/core';
import { Response } from '@angular/http';
import { PostHttpService } from '../httpServices/postHttpService';
import { UserHttpService } from '../httpServices/userHttpService';
import { Post } from '../models/post';
import { User } from '../models/user';
import { AppModule } from '../app.module';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-newPost-app',
    template: `    <div class="panel">
                        <div class="panel-heading">
                            <h3>Create new post</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Post Title:</label>
                                <input style="margin-left: 15px;width:500px" type="text" [(ngModel)]="_post.Title" name="tbPostTitle"/>
                            </div>
                            <div class="form-group">
                                <label>Post Article:</label>
                                <textarea style="width:500px;height:300px" [(ngModel)]="_post.Article" name="tbPostTitle"></textarea>
                            </div>
                            <div class="panel-footer">
                                <div>
                                    Input First Name:<input type="text" [(ngModel)]="_user.FirstName" /> Input Last Name:<input type="text" [(ngModel)]="_user.LastName" />
                                    Input Email:<input type="text" [(ngModel)]="_user.Email" />
                                </div>
                            </div>
                            <div style="padding-left:500px">
                                <input type="submit" name="tbSubmitCreateCategory" (click)="createNewPost(_user,_post)" value="Submit" class="btn btn-primary" />
                            </div>                        
                        </div>
                </div>`,
    providers: [PostHttpService, UserHttpService]
})
export class CreateNewPostComponent {

    _post: Post = new Post();
    _user: User = new User();
    constructor(private postHttpService: PostHttpService, private userHttpService: UserHttpService, private activateRoute: ActivatedRoute)
    {
        this._post.CategoryId = activateRoute.snapshot.params['categoryId'];
    }

    createNewPost(user: User, post: Post) {
        this.userHttpService.careateUserData(user)
            .subscribe((data) => {
                post.AuthorId = data.Id;
                this.postHttpService.createPostData(post).subscribe((data) => {
                    console.log(data.Id);
                    window.location.replace('currentPost/' + data.Id);
                })
            });
    }
}
