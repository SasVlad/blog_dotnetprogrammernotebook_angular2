"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const postHttpService_1 = require("../httpServices/postHttpService");
const userHttpService_1 = require("../httpServices/userHttpService");
const post_1 = require("../models/post");
const user_1 = require("../models/user");
const router_1 = require("@angular/router");
let CreateNewPostComponent = class CreateNewPostComponent {
    constructor(postHttpService, userHttpService, activateRoute) {
        this.postHttpService = postHttpService;
        this.userHttpService = userHttpService;
        this.activateRoute = activateRoute;
        this._post = new post_1.Post();
        this._user = new user_1.User();
        this._post.CategoryId = activateRoute.snapshot.params['categoryId'];
    }
    createNewPost(user, post) {
        this.userHttpService.careateUserData(user)
            .subscribe((data) => {
            post.AuthorId = data.Id;
            this.postHttpService.createPostData(post).subscribe((data) => {
                console.log(data.Id);
                window.location.replace('currentPost/' + data.Id);
            });
        });
    }
};
CreateNewPostComponent = __decorate([
    core_1.Component({
        selector: 'my-newPost-app',
        template: `    <div class="panel">
                        <div class="panel-heading">
                            <h3>Create new post</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Post Title:</label>
                                <input style="margin-left: 15px;width:500px" type="text" [(ngModel)]="_post.Title" name="tbPostTitle"/>
                            </div>
                            <div class="form-group">
                                <label>Post Article:</label>
                                <textarea style="width:500px;height:300px" [(ngModel)]="_post.Article" name="tbPostTitle"></textarea>
                            </div>
                            <div class="panel-footer">
                                <div>
                                    Input First Name:<input type="text" [(ngModel)]="_user.FirstName" /> Input Last Name:<input type="text" [(ngModel)]="_user.LastName" />
                                    Input Email:<input type="text" [(ngModel)]="_user.Email" />
                                </div>
                            </div>
                            <div style="padding-left:500px">
                                <input type="submit" name="tbSubmitCreateCategory" (click)="createNewPost(_user,_post)" value="Submit" class="btn btn-primary" />
                            </div>                        
                        </div>
                </div>`,
        providers: [postHttpService_1.PostHttpService, userHttpService_1.UserHttpService]
    }),
    __metadata("design:paramtypes", [postHttpService_1.PostHttpService, userHttpService_1.UserHttpService, router_1.ActivatedRoute])
], CreateNewPostComponent);
exports.CreateNewPostComponent = CreateNewPostComponent;
//# sourceMappingURL=createNewPostComponent.js.map