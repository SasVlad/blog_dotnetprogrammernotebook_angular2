"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const categoryHttpService_1 = require("../httpServices/categoryHttpService");
const category_1 = require("../models/category");
let CreateCategoryComponent = class CreateCategoryComponent {
    constructor(categoryHttpService) {
        this.categoryHttpService = categoryHttpService;
        this.newCategory = new category_1.Category();
    }
    createNewCategory(newCategory) {
        this.categoryHttpService.careateCategoryData(newCategory)
            .subscribe((data) => { window.location.replace('postsByCategory/' + data.Id); }); //window.location.replace('/#!/newCategory');
    }
};
CreateCategoryComponent = __decorate([
    core_1.Component({
        selector: 'my-createCategory-app',
        template: `<div class="panel">
                    <div class="panel-heading">
                        <h3>Create new category</h3>
                    </div>
                    <div class="panel-body">
                         <div class="form-group">
                            <label>Category Name:</label>
                            <input style="margin-left: 15px;width:500px" type="text" [(ngModel)]="newCategory.Name" name="tbCategoryName" />
                         </div>        
                         <div style="padding-left:500px">
                            <input type="submit" name="tbSubmitCreateCategory" (click)="createNewCategory(newCategory)" value="Submit" class="btn btn-primary" />
                         </div>
                    </div>
            </div>`,
        providers: [categoryHttpService_1.CategoryHttpService]
    }),
    __metadata("design:paramtypes", [categoryHttpService_1.CategoryHttpService])
], CreateCategoryComponent);
exports.CreateCategoryComponent = CreateCategoryComponent;
//# sourceMappingURL=createCategoryComponent.js.map