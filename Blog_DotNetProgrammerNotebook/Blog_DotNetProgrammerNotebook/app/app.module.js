"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const platform_browser_1 = require("@angular/platform-browser");
const router_1 = require("@angular/router");
const http_1 = require("@angular/http");
const forms_1 = require("@angular/forms");
const categoryComponent_1 = require("./components/categoryComponent");
const createCategoryComponent_1 = require("./components/createCategoryComponent");
const createNewPostComponent_1 = require("./components/createNewPostComponent");
const displayPostByIdComponent_1 = require("./components/displayPostByIdComponent");
const displayPostsByCategoryIdComponent_1 = require("./components/displayPostsByCategoryIdComponent");
// определение маршрутов
const appRoutes = [
    { path: '', redirectTo: "postsByCategory/1", pathMatch: 'full' },
    { path: 'postsByCategory/:categoryId', component: displayPostsByCategoryIdComponent_1.DisplayPostsByCategoryIdComponent },
    { path: 'newPost/:categoryId', component: createNewPostComponent_1.CreateNewPostComponent },
    { path: 'newCategory', component: createCategoryComponent_1.CreateCategoryComponent },
    { path: 'currentPost/:postid', component: displayPostByIdComponent_1.DisplayPostByIdComponent }
];
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, router_1.RouterModule.forRoot(appRoutes), http_1.HttpModule, forms_1.FormsModule],
        declarations: [categoryComponent_1.CategoryComponent, displayPostsByCategoryIdComponent_1.DisplayPostsByCategoryIdComponent, createNewPostComponent_1.CreateNewPostComponent,
            createCategoryComponent_1.CreateCategoryComponent, displayPostByIdComponent_1.DisplayPostByIdComponent],
        bootstrap: [categoryComponent_1.CategoryComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map