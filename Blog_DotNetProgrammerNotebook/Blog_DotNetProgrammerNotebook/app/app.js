var el = this.document.getElementById("content");
class User {
    constructor(_name, _age) {
        this.name = _name;
        this.age = _age;
    }
}
var tom = new User("Tom", 29);
el.innerHTML = "Name: " + tom.name + " age: " + tom.age;
//# sourceMappingURL=app.js.map