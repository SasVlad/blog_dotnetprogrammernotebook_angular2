﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers } from '@angular/http';
import { Post } from '../models/post';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class PostHttpService {

    constructor(private http: Http) { }

    createPostData(obj: Post) {
        const body = JSON.stringify(obj);

        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

        return this.http.post('/Post/CreatePost', body, { headers: headers })
            .map((resp: Response) => resp.json())
            .catch((error: any) => { return Observable.throw(error); });
    }

    getPostById(num: number) {
        return this.http.get('/Post/GetPostByIdJson?postid=' + num)
            .map((resp: Response) => resp.json())
            .catch((error: any) => { return Observable.throw(error); });
    }

    getAllPostsByCategoryId(num: number) {
        return this.http.get('/Post/GetAllPostsByCategoryIdJson?categoryId=' + num)
            .map((resp: Response) => resp.json())
            .catch((error: any) => { return Observable.throw(error); });
    }
}