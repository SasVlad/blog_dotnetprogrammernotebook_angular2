"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const http_2 = require("@angular/http");
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/throw");
let PostHttpService = class PostHttpService {
    constructor(http) {
        this.http = http;
    }
    createPostData(obj) {
        const body = JSON.stringify(obj);
        let headers = new http_2.Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        return this.http.post('/Post/CreatePost', body, { headers: headers })
            .map((resp) => resp.json())
            .catch((error) => { return Observable_1.Observable.throw(error); });
    }
    getPostById(num) {
        return this.http.get('/Post/GetPostByIdJson?postid=' + num)
            .map((resp) => resp.json())
            .catch((error) => { return Observable_1.Observable.throw(error); });
    }
    getAllPostsByCategoryId(num) {
        return this.http.get('/Post/GetAllPostsByCategoryIdJson?categoryId=' + num)
            .map((resp) => resp.json())
            .catch((error) => { return Observable_1.Observable.throw(error); });
    }
};
PostHttpService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PostHttpService);
exports.PostHttpService = PostHttpService;
//# sourceMappingURL=postHttpService.js.map