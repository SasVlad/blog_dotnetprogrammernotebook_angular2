﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CategoryComponent } from './components/categoryComponent';
import { CreateCategoryComponent } from './components/createCategoryComponent';
import { CreateNewPostComponent } from './components/createNewPostComponent';
import { DisplayPostByIdComponent } from './components/displayPostByIdComponent';
import { DisplayPostsByCategoryIdComponent } from './components/displayPostsByCategoryIdComponent';

// определение маршрутов
const appRoutes: Routes = [
    { path: '', redirectTo: "postsByCategory/1", pathMatch: 'full'},
    { path: 'postsByCategory/:categoryId', component: DisplayPostsByCategoryIdComponent },
    { path: 'newPost/:categoryId', component: CreateNewPostComponent },
    { path: 'newCategory', component: CreateCategoryComponent },
    { path: 'currentPost/:postid', component: DisplayPostByIdComponent }
];

@NgModule({
    imports: [BrowserModule, RouterModule.forRoot(appRoutes), HttpModule, FormsModule],
    declarations: [CategoryComponent, DisplayPostsByCategoryIdComponent, CreateNewPostComponent,
        CreateCategoryComponent, DisplayPostByIdComponent],
    bootstrap: [CategoryComponent]
})
export class AppModule { }