﻿import { User } from '../models/user';
export class Comment {
    Id: number;
    PostId: number;
    UserId: number;
    CommentText: string;
    DateRecord: Date;
    User: User;
}