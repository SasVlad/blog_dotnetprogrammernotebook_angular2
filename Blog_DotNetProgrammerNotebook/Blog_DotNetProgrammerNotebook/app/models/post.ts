﻿import { User } from '../models/user';
import { Comment } from '../models/comment';
export class Post {
    constructor() {
        this.Author = new User();
        this.Comments = [];
    }
    Id: number;
    Title: string;
    Article: string;
    AuthorId: string;
    CategoryId: string;
    DatePublished: Date;
    Views: number;
    Author: User;
    Comments: Comment[];
} 