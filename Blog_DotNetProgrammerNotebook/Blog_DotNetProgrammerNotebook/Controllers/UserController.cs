﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class UserController : Controller
    {
        UserService _userService = new UserService();
        // GET: User
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            return View(_userService.GetAllUsersAsync());
        }
        // ?? 
        [HttpGet]
        public ActionResult GetUserById(int userid)
        {
            var _result = _userService.GetUserByIdAsync(userid);

            return View();
        }
        [HttpGet]
        public ActionResult CreateUser()
        {

            return View();
        }
        [HttpPost]
        public async Task<JsonResult> CreateUser(User user)
        {
            var result = await _userService.CreateUserAsync(user);

            if (result != null)
            {
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            return Json(new User());


        }
        [HttpGet]
        public ActionResult DeleteUserById(int userid)
        {
            _userService.DeleteUserByIdAsync(userid);
            return RedirectToAction("GetAllUsers");
        }
        [HttpPost]
        public ActionResult UpdateUser(User user)
        {
            var result = _userService.UpdateUserAsync(user);
            if (result != null)
            {
                return RedirectToAction("GetAllUsers");
            }
            return View();
        }
    }
}