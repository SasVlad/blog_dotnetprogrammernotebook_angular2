﻿using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class HomeController : Controller
    {
        UserService _userService = new UserService();
        CategoryService _categoryService = new CategoryService();
        CommentService _commentService = new CommentService();
        PostService _postService = new PostService();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> TestIndex()
        {    
            return View();
        }
    }
}