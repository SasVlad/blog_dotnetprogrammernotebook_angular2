﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class CategoryController : Controller
    {
        CategoryService _categoryService = new CategoryService();
        // GET: Category
        public async Task<JsonResult> GetAllCategories()
        {
            return Json( await _categoryService.GetAllCategoriesAsync(),JsonRequestBehavior.AllowGet);
        }
        // ?? 
        [HttpGet]
        public async Task<JsonResult> GetCategoryById(int categoryid)
        {
            var _result = await _categoryService.GetCategoryByIdAsync(categoryid);
            return Json(_result,JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult CreateCategory()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> CreateCategory(Category category)
        {
            category.DateCreated = DateTime.Now;
            var result = await  _categoryService.CreateCategoryAsync(category);

            if (result!=null)
            {
                return Json(result,JsonRequestBehavior.AllowGet);
            }
                return Json(new Category(), JsonRequestBehavior.AllowGet);
            
            
        }
        [HttpGet]
        public ActionResult DeleteCategoryById(int categoryid)
        {
            _categoryService.DeleteCategoryByIdAsync(categoryid);
            return RedirectToAction("GetAllCategories");
        }
        [HttpPost]
        public ActionResult UpdateCategory(Category category)
        {
            var result = _categoryService.UpdateCategoryAsync(category);
            if (result != null)
            {
                return RedirectToAction("GetAllCategories");
            }
            return View();
        }
    }
}