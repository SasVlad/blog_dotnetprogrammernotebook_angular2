﻿using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace Blog_DotNetProgrammerNotebook.EF
{
    internal class BlogDatabaseInitialize:CreateDatabaseIfNotExists<BlogContext>
    {
        public BlogDatabaseInitialize()
        {
        }
        protected override void Seed(BlogContext context)
        {
            var _user1 = new User { Email = "hi@gmail.com", FirstName = "Author", LastName = "Lomakin" };            
            var _user2 = new User { Email = "user1@gmail.com", FirstName = "User1", LastName = "Userovich1" };
            var _user3 = new User { Email = "user2@gmail.com", FirstName = "User2", LastName = "Userovich2" };
            var _user4 = new User { Email = "user3@gmail.com", FirstName = "User3", LastName = "Userovich3" };

            context.Users.Add(_user1);
            context.Users.Add(_user2);
            context.Users.Add(_user3);
            context.Users.Add(_user4);
            context.SaveChanges();

            var _categoryMvc = new Category { Name = "Asp.Net MVC", DateCreated = DateTime.Now };
            var _categoryWebApi = new Category { Name = "Asp.Net WebApi2", DateCreated = DateTime.Now };
            var _categoryCSharp = new Category { Name = "C#", DateCreated = DateTime.Now };
            var _categoryAngular = new Category { Name = "Angular", DateCreated = DateTime.Now };
            context.Categories.Add(_categoryMvc);
            context.Categories.Add(_categoryWebApi);
            context.Categories.Add(_categoryCSharp);
            context.Categories.Add(_categoryAngular);
            context.SaveChanges();

            var _post = new Post { Article = "Asp.net MVC is the best serverside engine", AuthorId = _user1.Id,
                Title = "Introduce in Asp.net MVC", Views = 0, DatePublished = DateTime.Now, CategoryId = _categoryMvc.Id};
            var _post1 = new Post
            {
                Title = "Using Sessions and HttpContext in ASP.NET 5 and MVC6",
                AuthorId = _user1.Id,
                Article = @"State management is very important and useful concept in Web application 
                and also its equally very important that resources can not be allocated unnecessarily.
                In ASP.NET MVC we can manage the session controller specific which helps to disable the session when you don't require IT
                for particular controller and due to this we can improve the performance of an application by
                freeing resources not needed to be allocated. Now you might be thinking that how it will be possible?",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryMvc.Id
            };
            var _post2 = new Post
            {
                Title = "Layout View in ASP.NET MVC",
                AuthorId = _user1.Id,
                Article = @"An application may contain common parts in the UI which remains the same throughout
                the application such as the logo, header, left navigation bar, right bar or footer section.
                ASP.NET MVC introduced a Layout view which contains these common UI parts,
                so that we don't have to write the same code in every page.
                The layout view is same as the master page of the ASP.NET webform application.",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryMvc.Id
            };
            var _post3 = new Post
            {
                Title = "Secure a Web API with Individual Accounts and Local Login in ASP.NET Web API 2.2",
                AuthorId = _user1.Id,
                Article = @"In Visual Studio 2013, the Web API project template gives you three options for authentication:
                Individual accounts.The app uses a membership database.
                Organizational accounts.Users sign in with their Azure Active Directory,
                Office 365,
                or on - premise Active Directory credentials.
                Windows authentication.This option is intended for Intranet applications, and uses the Windows Authentication IIS module.",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryWebApi.Id
            };
            var _post4= new Post
            {
                Title = "Introduction to ASP.NET Identity",
                AuthorId = _user1.Id,
                Article = @"The ASP.NET membership system was introduced with ASP.NET 2.0 back
                in 2005, and since then there have been many changes in the ways web applications typically
                handle authentication and authorization. ASP.NET Identity is a fresh look at what the membership
                system should be when you are building modern applications for the web, phone, or tablet.",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryWebApi.Id
            };
            var _post5 = new Post
            {
                Title = "OR, AND Operator",
                AuthorId = _user1.Id,
                Article = @"There is a distinction between the conditional operators && and || and the boolean operators
                & and |. Mainly it is a difference of precendence (which operators get evaluated first) and also the && and || are 'escaping'.
                This means that is a sequence such as...
                cond1 && cond2 && cond3
                If cond1 is false, neither cond2 or cond3 are evaluated as the code rightly assumes 
                that no matter what their value, the expression cannot be true. Likewise...
                cond1 || cond2 || cond3
                If cond1 is true, neither cond2 or cond3 are evaluated as the expression must be true no matter what their value is.
                The bitwise counterparts, & and | are not escaping.",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryCSharp.Id
            };
            var _post6 = new Post
            {
                Title = "AngularJS Introduction",
                AuthorId = _user1.Id,
                Article = @"AngularJS is a JavaScript framework. It can be added to an HTML page with a <script> tag.
                AngularJS extends HTML attributes with Directives, and binds data to HTML with Expressions.
                AngularJS is a JavaScript Framework
                AngularJS is a JavaScript framework. It is a library written in JavaScript.
                AngularJS is distributed as a JavaScript file, and can be added to a web page with a script tag:
                <script src=https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js></script>",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryAngular.Id
            };
            var _post7 = new Post
            {
                Title = "AngularJS - MVC Architecture",
                AuthorId = _user1.Id,
                Article = @"Model View Controller or MVC as it is popularly called, is a software design pattern
                for developing web applications. A Model View Controller pattern is made up of the following three parts −
                Model − It is the lowest level of the pattern responsible for maintaining data.
                View − It is responsible for displaying all or a portion of the data to the user.
                Controller − It is a software Code that controls the interactions between the Model and View.",
                Views = 0,
                DatePublished = DateTime.Now,
                CategoryId = _categoryAngular.Id
            };

            context.Posts.Add(_post);
            context.Posts.Add(_post1);
            context.Posts.Add(_post2);
            context.Posts.Add(_post3);
            context.Posts.Add(_post4);
            context.Posts.Add(_post5);
            context.Posts.Add(_post6);
            context.Posts.Add(_post7);
            context.SaveChanges();

            context.Comments.Add(new Comment {PostId= _post.Id , UserId=_user2.Id,CommentText="This is cool post. Thanks! =)",DateRecord=DateTime.Now});
            context.Comments.Add(new Comment { PostId = _post.Id, UserId = _user3.Id, CommentText = "Thanks! You are the best=)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post1.Id, UserId = _user4.Id, CommentText = "Amazing. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post1.Id, UserId = _user2.Id, CommentText = "This is cool article. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post2.Id, UserId = _user3.Id, CommentText = "Good post. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post2.Id, UserId = _user4.Id, CommentText = "Just hanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post3.Id, UserId = _user2.Id, CommentText = "Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post3.Id, UserId = _user3.Id, CommentText = "Yeah Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post3.Id, UserId = _user4.Id, CommentText = "Hmm... I don't know this. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post4.Id, UserId = _user2.Id, CommentText = "This is cool post. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post4.Id, UserId = _user3.Id, CommentText = "Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post5.Id, UserId = _user4.Id, CommentText = "This is cool post. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post5.Id, UserId = _user2.Id, CommentText = "Good post. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post6.Id, UserId = _user3.Id, CommentText = "Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post6.Id, UserId = _user4.Id, CommentText = "This is cool article. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post7.Id, UserId = _user2.Id, CommentText = "Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post7.Id, UserId = _user3.Id, CommentText = "Hmm... I don't know this. Thanks! =)", DateRecord = DateTime.Now });
            context.Comments.Add(new Comment { PostId = _post7.Id, UserId = _user4.Id, CommentText = "Thanks! =)", DateRecord = DateTime.Now });
            base.Seed(context);
        }
    }
}