﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Models
{
    public class Post
    {
        public Post()
        {
            Comments = new List<Comment>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Article { get; set; }
        public int? AuthorId { get; set; }
        public int CategoryId { get; set; }
        public DateTime DatePublished { get; set; }
        public int Views { get; set; }

        //public string BannerImage { get; set; }
        public  User Author { get; set; }
        public  Category Category { get; set; }
        public  ICollection<Comment> Comments { get; set; }
    }
}