﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
    }
}