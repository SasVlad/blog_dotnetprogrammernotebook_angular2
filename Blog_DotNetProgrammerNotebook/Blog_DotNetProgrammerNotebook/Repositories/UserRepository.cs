﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class UserRepository
    {
        //public UserRepository()
        //{
        //    using (var ctx = new BlogContext())
        //    {
        //        using (var writer = new XmlTextWriter($@"{AppDomain.CurrentDomain.BaseDirectory}\BlogEDM.edmx", Encoding.Default))
        //        {
        //            EdmxWriter.WriteEdmx(ctx, writer);
        //        }
        //    }
        //}
        public async Task<List<User>> GetAllUsersAsync()
        {
            var _listUsers = new List<User>();
            using (var context = new BlogContext())
            {
                _listUsers = await context.Users.ToListAsync();
            }
            return _listUsers;
        }
        public async Task<User> GetUserByIdAsync(int Id)
        {
            var _user = new User();
            using (var context = new BlogContext())
            {
                _user = await context.Users.FirstOrDefaultAsync(x => x.Id == Id);
            }
            return _user;
        }

        public async Task<User> CreateUserAsync(User user)
        {
            var _user = new User();
            using (var context = new BlogContext())
            {
                _user = await context.Users.FirstOrDefaultAsync(x => x.Email == user.Email);

                if (_user == null)
                {
                    context.Users.Add(user);
                    await context.SaveChangesAsync();
                    _user = user;
                }
            }
            return _user;
        }

        public async void DeleteUserByIdAsync(int id)
        {
            var _user = new User();
            using (var context = new BlogContext())
            {
                _user = await context.Users.FirstOrDefaultAsync(x => x.Id == id);

                if (_user != null)
                {
                    context.Users.Remove(_user);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            var _user = new User();
            using (var context = new BlogContext())
            {
                _user = await context.Users.FirstOrDefaultAsync(x => x.Id == user.Id);

                if (_user != null)
                {
                    context.Users.AddOrUpdate(user);
                    await context.SaveChangesAsync();
                    _user = user;
                }
            }
            return _user;
        }
    }
}