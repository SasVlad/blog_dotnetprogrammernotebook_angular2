﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class PostRepository
    {
        public async Task<List<Post>> GetAllPostsAsync()
        {
            var _listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                _listPosts = await context.Posts
                    .Include(x => x.Author).Include(x => x.Category).Include(x => x.Comments).ToListAsync();
            }
            return _listPosts;
        }
        public async Task<List<Post>> GetAllPostsByCategoryIdAsync(int categoryId)
        {
            var _listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                _listPosts = await context.Posts.Where(x=>x.CategoryId==categoryId)
                    .Include(x => x.Author).Include(x => x.Category).ToListAsync();
            }
            return _listPosts;
        }
        public async Task<List<Post>> GetAllPostsByAuthorIdAsync(int authorId)
        {
            var _listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                _listPosts = await context.Posts.Where(x => x.AuthorId == authorId)
                .Include(x=>x.Author).Include(x=>x.Category).ToListAsync();
            }
            return _listPosts;
        }
        public async Task<Post> GetPostByIdAsync(int Id)
        {
            var _post = new Post();
            using (var context = new BlogContext())
            {
                _post = await context.Posts.FirstOrDefaultAsync(x => x.Id == Id);
                await context.Entry(_post).Reference("Author").LoadAsync();
                await context.Entry(_post).Reference("Category").LoadAsync();
                await context.Entry(_post).Collection("Comments").Query().Include("User").LoadAsync();
            }
            return _post;
        }

        public async Task<Post> CreatePostAsync(Post post)
        {
            var _post = new Post();
            using (var context = new BlogContext())
            {
                _post = await context.Posts.FirstOrDefaultAsync(x => x.Article == post.Article);

                if (_post == null)
                {
                    context.Posts.Add(post);
                    await context.SaveChangesAsync();
                    _post = post;
                }
            }
            return _post;
        }

        public async void DeletePostByIdAsync(int id)
        {
            var _post = new Post();
            using (var context = new BlogContext())
            {
                _post = await context.Posts.FirstOrDefaultAsync(x => x.Id == id);

                if (_post != null)
                {
                    context.Posts.Remove(_post);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Post> UpdatePostAsync(Post post)
        {
            var _post = new Post();
            using (var context = new BlogContext())
            {
                _post = await context.Posts.FirstOrDefaultAsync(x => x.Id == post.Id);

                if (_post != null)
                {
                    context.Posts.AddOrUpdate(post);
                   await context.SaveChangesAsync();
                    _post = post;
                }
            }
            return _post;
        }
    }
}