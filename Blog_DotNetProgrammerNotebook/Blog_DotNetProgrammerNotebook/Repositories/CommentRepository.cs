﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class CommentRepository
    {
        public async Task<List<Comment>> GetAllCommentsAsync()
        {
            var _listComments = new List<Comment>();
            using (var context = new BlogContext())
            {
                _listComments= await context.Comments.Include(x => x.User).ToListAsync();
            }
            return _listComments;
        }
        public async Task<List<Comment>> GetAllCommentsByPostIdAsync(int postId)
        {
            var _listComments = new List<Comment>();
            using (var context = new BlogContext())
            {
                _listComments = await context.Comments.Include(x => x.User).ToListAsync();
            }
            return _listComments;
        }
        public async Task<Comment> GetCommentByIdAsync(int Id)
        {
            var _comment = new Comment();
            using (var context = new BlogContext())
            {
                _comment = await context.Comments.FirstOrDefaultAsync(x => x.Id == Id);
                await context.Entry(_comment).Reference("User").LoadAsync();
            }
            return _comment;
        }

        public async Task<Comment> CreateCommentAsync(Comment comment)
        {
            var _comment = new Comment();
            using (var context = new BlogContext())
            {
                _comment = comment;

                if (_comment != null)
                {
                    context.Comments.Add(comment);
                    await context.SaveChangesAsync();
                    _comment = comment;
                }
            }
            return _comment;
        }

        public async void DeleteCommentByIdAsync(int id)
        {
            var _comment = new Comment();
            using (var context = new BlogContext())
            {
                _comment = await context.Comments.FirstOrDefaultAsync(x => x.Id == id);

                if (_comment != null)
                {
                    context.Comments.Remove(_comment);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Comment> UpdateCommentAsync(Comment comment)
        {
            var _comment = new Comment();
            using (var context = new BlogContext())
            {
                _comment = await context.Comments.FirstOrDefaultAsync(x => x.Id == comment.Id);

                if (_comment != null)
                {
                    context.Comments.AddOrUpdate(comment);
                    await context.SaveChangesAsync();
                    _comment = comment;
                }
            }
            return _comment;
        }
    }
}